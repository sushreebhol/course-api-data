package com.test.springbootstarter.products;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductService {

	
	@Autowired
	private ProductsRepository productsRepository;
	
	/*private List<Product> products =  new ArrayList<>(Arrays.asList(
			
			new Product("Spring","Spring Framework","Spring Framework Description"),
			new Product("Java","java Framework","java Framework Description"),
			new Product("javascript","javascript Framework","javascript Framework Description")
			));*/
	
	
	public List<Product> getAllProducts(){
		List<Product> products = new ArrayList<>();
		productsRepository.findAll().forEach(products::add);
		
		return products;
	}
	
	public Product getProduct(String id) {
		return productsRepository.findById(id).get();
	}
	
	public void addProduct(Product product) {
		productsRepository.save(product);
	}
	
	public void updateProduct(String id, Product product) {
		
		productsRepository.save(product);
	}
	
	public void deleteProduct(String id) {
		
		productsRepository.deleteById(id);
		
	}
}
