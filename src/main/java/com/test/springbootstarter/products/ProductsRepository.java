package com.test.springbootstarter.products;

import org.springframework.data.repository.CrudRepository;

public interface ProductsRepository  extends CrudRepository<Product,String>{

}
